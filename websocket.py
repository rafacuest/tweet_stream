from geventwebsocket.handler import WebSocketHandler
from gevent import pywsgi
import gevent
import time

def app(environ, start_response):
	ws = environ['wsgi.websocket']
	contador = 0
	while True:
		strTemp = "hola" + str(contador) 
		ws.send( strTemp )
		time.sleep(1)
		contador = contador + 1
		
server = pywsgi.WSGIServer(('', 10000), app, handler_class=WebSocketHandler)
server.serve_forever()

