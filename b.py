import gevent
import gevent.monkey
gevent.monkey.patch_all()

from geventwebsocket.handler import WebSocketHandler
from gevent import pywsgi

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json

access_token = "306824124-53IicRhYqsqhU2CZnJcthVQnZor1hBOPIafsIYlf"
access_token_secret = "q4QW5RqYnxKoBdTzloLFV4s8QLU2pkpOOOmASXKe8tYdG"
consumer_key = "QSHJk57P6rPA5Ov3OajPg"
consumer_secret = "6A5eOAEcysW6nyxC04YeL7IhOu97k0YOycltouJoY"

class MyStreamListener(StreamListener):
    def __init__(self):
		self.geobox_world = [-180,-90,180,90]
		self.geobox_spain =[-11.757813,35.340503,5.644531,43.217838]
		self.geobox_catalunya = [0.266724,40.447627,3.562622,42.763802]
		self.sockets = []
		auth = OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		self.stream = Stream(auth, self)
	
    def add_socket(self, ws):
        self.sockets.append(ws)

    def run(self):
        try:
            self.stream.filter(locations=self.geobox_catalunya)
        except Exception:
            self.stream.disconnect()

    def start(self):
        gevent.spawn(self.run)

    def send(self, ws, coordinates):
        try:
            ws.send(json.dumps(coordinates))
        except Exception:
            # the web socket die..
            self.sockets.remove(ws)

    def on_data(self, data):
        decoded = json.loads(data)
        self.output = open('data/s.json', 'a+')
        self.output.write(json.dumps(decoded)+"\n")
        if decoded.get("coordinates", None) is not None:
            coordinates = decoded["coordinates"]["coordinates"]
            #print coordinates
            #coordinates2[0] = coordinates[1]
            #coordinates2[1] = coordinates[0]
            #print coordinates2 
            for ws in self.sockets:
                #gevent.spawn(self.send, ws, coordinates)
                gevent.spawn(self.send, ws, decoded)
                
        return True

    def on_error(self, status):
        print "Error", status

    def on_timeout(self):
        print "tweepy timeout.. wait 30 seconds"
        gevent.sleep(30)

stream_listener = MyStreamListener()
stream_listener.start()


def app(environ, start_response):
    ws = environ['wsgi.websocket']
    stream_listener.add_socket(ws)
    while not ws.closed:
        gevent.sleep(0.1)

server = pywsgi.WSGIServer(('', 10000), app, handler_class=WebSocketHandler)
server.serve_forever()
